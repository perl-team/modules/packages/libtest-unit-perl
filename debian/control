Source: libtest-unit-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Xavier Guimard <yadd@debian.org>,
           Axel Beckert <abe@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: libclass-inner-perl <!nocheck>,
                     libdevel-symdump-perl <!nocheck>,
                     liberror-perl <!nocheck>,
                     perl
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libtest-unit-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libtest-unit-perl.git
Homepage: https://metacpan.org/release/Test-Unit
Rules-Requires-Root: no

Package: libtest-unit-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libclass-inner-perl,
         libdevel-symdump-perl,
         liberror-perl
Suggests: perl-tk
Description: unit testing framework for Perl
 Test::Unit::* is a sophisticated unit testing framework for Perl
 that is derived from the JUnit testing framework for Java by Kent
 Beck and Erich Gamma.
 .
 While this framework is originally intended to support unit
 testing in an object-oriented development paradigm (with support
 for inheritance of tests etc.), Test::Unit::Procedural is intended
 to provide a simpler interface to the framework that is more
 suitable for use in a scripting style environment.
